var express  = require('express');
var auth = require('basic-auth-connect');
var app = express();
var path     = require('path');
var compress = require('compression');
var logger   = require('morgan');
var bodyParser = require('body-parser');

var settings = {
  root: path.resolve(process.cwd(), 'public'),
  port: process.env.PORT || 5000,
  logLevel: process.env.NODE_ENV ? 'combined' : 'dev'
};

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use(compress()).use(logger(settings.logLevel));

var port = process.env.PORT || 7327;

app.use(express.static(settings.root));

app.listen(port, function(){
	console.log('Start server @', port);
});

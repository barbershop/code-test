# Code Challenges
This is a test in which candidates will try to complete as many challenges as possible in 2 hours. There is no particular order and you may start or finish them in any order you choose. These challenges are simply meant to gauge your level of knowledge in certain aspects web development. Do your best to provide the most efficient and intelligent solution to each problem. Upon completion, please be prepared to review your code with us and explain your solutions. Also if you are unable to solve any part of a challenge, please feel free to ask questions after the test is complete. Thanks!

## Prerequisites

* Node.js (https://nodejs.org/en/)

## To start

Clone the repo to your local machine.

```
$ cd code-test
$ npm install
$ npm run develop
```

Please use ```git``` and commit code as you go along.

Have fun!
